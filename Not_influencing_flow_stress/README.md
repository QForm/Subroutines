# Not influencing flow stress subroutines

Subroutines not influencing flow stress are needed to calculate the criteria and fields that help to further analyze the resultes of main simulation.

For loading the file with subroutine code go to **Subroutines** tab in **Initial data** bar and press **Add subroutine**. The \*.lua file may be located any place on the hard disc.

![QForm interface for postprocessing subroutines](interface.png)

So that the subroutine written in the Lua language can correctly interact with the [QForm](http://qform3d.com) program the following steps should be done:
 - Indicate for what object a simulation will be conducted. For a forged part – `set_target_workpiece()`, for tools – `set_target_tool()`, for extrusion trace in [QForm Extrusion](http://www.qform3d.com/products/qformextrusion) – `set_target_extrusion_trace()`;
 - Specify user-defined fields by the function `{varible_name} = result({varible_name})`;
 - If required, specify additional input parameters by the function `{parameter_name} = parameter({parameter_name}, {parameter_default_value})`;
 - Create the function `UserFields()`, in which user-defined fields will be computed;
 - Use function `store({varible_name}, {local_varible_name})` into `UserFields()` function for sending variables values to [QForm](http://qform3d.com).

>**Important:** Any calculation of user-defined fields for tools is possible only with **coupled deformation** task