--The subroutine is intended for cooling rate calculation

set_target_workpiece()

cooling_rate        = result("Cooling rate current [Celsius/s]", 0) --cooling rate, current [Celsius per second]
cooling_rate_common = result("Cooling rate common [Celsius/s]", 0) --cooling rate, average [Celsius per second]

T_ini = result("T_ini", 0) --the initial process temperature [Celsius]

function UserFields (T, prev_T, prev_T_ini, t, dt)
	if t == 0 then
		T_ini_current  = T
		
		cooling_rate_current   = 0
		cooling_rate_common_current = 0
	else
		T_ini_current  = prev_T_ini
		
		cooling_rate_current   = (prev_T - T) / dt
		cooling_rate_common_current = (T_ini_current - T) / t
	end

	store (cooling_rate, cooling_rate_current)
	store (cooling_rate_common, cooling_rate_common_current)
	store (T_ini, T_ini_current)
end