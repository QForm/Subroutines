--The subroutine is intended for converting velocity in Cartesian system to the cylindrical coordinate system

set_target_workpiece()

velocity_radial = result ("velocity_radial", 0)
velocity_axial  = result ("velocity_axial",  0)
velocity_tang   = result ("velocity_tang",   0)

x1 = parameter("x1", 0)
y1 = parameter("y1", 0)
z1 = parameter("z1", 0)
x2 = parameter("x2", 0)
y2 = parameter("y2", 0)
z2 = parameter("z2", 1)

function UserFields (x,y,z,v_x, v_y, v_z)

    Vector = {}
    Vector.__index = Vector
    function Vector.__sub(a, b) -- subtraction
      if type(a) == "number" then
        return Vector.new(b.x - a, b.y - a, b.z - a)
      elseif type(b) == "number" then
        return Vector.new(a.x - b, a.y - b, a.z - b)
      else
        return Vector.new(a.x - b.x, a.y - b.y, a.z - b.z)
      end
    end

    function Vector.__mul(a, b) -- product
      if type(a) == "number" then
        return Vector.new(b.x * a, b.y * a, b.z * a)
      elseif type(b) == "number" then
        return Vector.new(a.x * b, a.y * b, a.z * b)
      else
        return Vector.new(a.x * b.x, a.y * b.y, a.z * b.z)
      end
    end

    function Vector.__mulscal(a, b) -- scalar product (the product of a vector by a unit vector is equal to its projection in the direction of the unit vector)
      return a.x * b.x + a.y * b.y + a.z * b.z
    end

    function Vector.__mulvect(a, b) -- cross product (perpendicular to the two vectors)
      return Vector.new(a.z * b.y - a.y * b.z, a.x * b.z - a.z * b.x, a.y * b.x - a.x * b.y)
    end

    function Vector.__div(a, b) -- division
      if type(a) == "number" then
        return Vector.new(b.x / a, b.y / a, b.z / a)
      elseif type(b) == "number" then
        return Vector.new(a.x / b, a.y / b, a.z / b)
      else
        return Vector.new(a.x / b.x, a.y / b.y, a.z / b.z)
      end
    end

    function Vector.new(x, y, z)    -- vector creation
      return setmetatable({ x = x or 0, y = y or 0,  z = z or 0}, Vector)
    end

    function Vector:len()   -- vector length
      return math.sqrt(self.x * self.x + self.y * self.y + self.z * self.z)
    end

    function Vector:normalize() -- vector normalization
      return self / self:len()
    end
     
    setmetatable(Vector, { __call = function(_, ...) return Vector.new(...) end })

	-- Comments on notation
	-- 1 - 1st point on the axis
	-- 2 - The 2nd set point on the axis
	-- 3 - Current node of FE mesh
	-- 5 - Projection of point 3 to the axis

    v1    = Vector.new(x1, y1, z1)
    v2    = Vector.new(x2, y2, z2)
    v1_2  = Vector.__sub(v1, v2)
    v1_2n = v1_2:normalize() -- the unit axial vector

    v3    = Vector.new(x, y, z)
    v1_3  = Vector.__sub(v1, v3)
    v4    = Vector.__mulvect(v1_3, v1_2)
    v4n   = v4:normalize() -- the unit tangential vector

    v1_5  = Vector.__mul(Vector.__mulscal(v1_3, v1_2n), v1_2n)
    v5_3  = Vector.__sub(v1_3, v1_5)
    v5_3n = v5_3:normalize() -- the unit radial vector

    velocity = Vector.new(v_x, v_y, v_z) -- velocity vector at the node

	if v5_3:len() == 0  -- if the node located on the axis
	then
		velocity_R = 0
		velocity_T = 0
	else
		velocity_R = Vector.__mulscal(velocity, v5_3n) -- the radial velocity
		velocity_T = Vector.__mulscal(velocity, v4n)   -- the tangential velocity		
	end

    velocity_A = Vector.__mulscal(velocity, v1_2n) -- the axial velocity
    
    store(velocity_radial, velocity_R * -1e3)
    store(velocity_axial, velocity_A * -1e3)
    store(velocity_tang, velocity_T * 1e3)
end