--The subroutine is intended for maximum and minimum temperature field in operation calculation

set_target_workpiece()

maxT = result ("maxT", -1e30)   --specify the parameter and its initial value
minT = result ("minT",  1e30)

function UserFields (T, prev_maxT, prev_minT)
	if (T > prev_maxT) then     --if the current temperature is greater than the previous maximum
		store(maxT, T)          --save the current temperature in maxT
	else
		store(maxT, prev_maxT)  --the maximum temperature is equal to the previous maximum
	end

	if (T < prev_minT) then     --if the current temperature is less than the previous minimum
		store(minT, T)          --save the current temperature in minT
	else
		store(minT, prev_minT)  --minimum temperature equal to the previous minimum
	end
end