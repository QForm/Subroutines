--The subroutine is intended for maximum and minimum temperature field in operation calculation

set_target_workpiece()

maxT = result ("maxT", -1e30)   --specify the parameter and its initial value
minT = result ("minT",  1e30)

function UserFields (T, prev_maxT, prev_minT)
	store(maxT, math.max(T, prev_maxT)) --the largest value of T and prev_maxT save in maxT 
	store(minT, math.min(T, prev_minT)) --the largest value of T and prev_minT save in mminT 
end