--The subroutine is intended for the Zener-Hollomon criterion calculation

set_target_workpiece()
--Activation energy coefficients
c1 = parameter("C1", 156000)
c2 = parameter("C2", 0)
c3 = parameter("C3", 0)
R = 8.314 --gas constant

Zener = result("Zener")
LogZenerMax = result("LogZenerMax", -1e30)

function UserFields (T, strain, strain_rate, prev_LogZenerMax)
	T = T + 273.15                    --conversion temperature in Kelvin
	Q = c1 + c2*T + c3*strain         --calculation of activation energy
	
	z = strain_rate*math.exp(Q/(R*T)) --calculation of the Zener-Hollomon criterion
	lz = math.log(z) --natural logarithm. math.log(z, 10) – logarithm
	lz_max = math.max(prev_LogZenerMax, lz) --save the maximum value of prev_LogZenerMax and lz
	
	--sent values to QForm
	store(Zener, lz)
	store(LogZenerMax, lz_max)
end