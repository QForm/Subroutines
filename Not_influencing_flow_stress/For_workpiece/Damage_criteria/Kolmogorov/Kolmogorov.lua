--The subroutine is intended for the Kolmogorov's damage criterion calculation

set_target_workpiece()

Kolmogorov = result ("Kolmogorov") --Kolmogorov's criterion
dKolmogorov = result ("dKolmogorov") --increment Kolmogorov's criterion in step
Tension_indicator = result ("Tension_indicator")
Limit_strain = result ("Limit_strain") --current limiting strain
Max_Tension_indicator = result ("Max_Tension_indicator", -1e30) --maximum tension indicator of the process
Min_Tension_indicator = result ("Min_Tension_indicator", 1e30) --minimum tension indicator of the process

--The points from the diagram of plasticity
eta1 = parameter("eta1", -2.4)
limit_strain1 = parameter("limit_strain1", 2.5)
eta2 = parameter("eta2", 0)
limit_strain2 = parameter("limit_strain2", 1.25)
eta3 = parameter("eta3", 1.2)
limit_strain3 = parameter("limit_strain3", 0.8)
eta4 = parameter("eta4", 2.4)
limit_strain4 = parameter("limit_strain4", 0.5)
eta5 = parameter("eta5", -18)
limit_strain5 = parameter("limit_strain5", 12)

--Create a table
limit_strain_table = {
{eta1,limit_strain1},
{eta2,limit_strain2},
{eta3,limit_strain3},
{eta4,limit_strain4},
{eta5,limit_strain5}}

--Remove the string if the limit strain is less than or equal to zero
for i = #limit_strain_table, 1 , -1 do
	if limit_strain_table[i][2] <= 0 then
		table.remove (limit_strain_table, i)
	end
end

--Sort table rows ascending eta
table.sort(limit_strain_table, function(a, b) return a[1] < b[1] end)


function GetInterpolatedValue(sorted_table, argument) --the function of linear interpolation
	local value
	
	for i = 1, #sorted_table - 1 do
		if argument <= sorted_table[i][1]
		then --extrapolation by the horizontal line
			return sorted_table[i][2]
		end
		
		if argument >= sorted_table[i + 1][1]
		then --extrapolation by the horizontal line
			value = sorted_table[i + 1][2]
		end
		
		if argument > sorted_table[i][1] and argument < sorted_table[i + 1][1]
		then --interpolation within the given points
			local delta_2_colum = sorted_table[i + 1][2] - sorted_table[i][2]
			local delta_1_colum = sorted_table[i + 1][1] - sorted_table[i][1]
			local argument_inc = argument - sorted_table[i][1]
			local ratio = argument_inc / delta_1_colum
			local value_inc = ratio * delta_2_colum
			value = value_inc + sorted_table[i][2]
			return value
		end
	end
	return value
end

function UserFields(stress_flow, stress_mean, prev_strain_rate, prev_Kolmogorov,
dt, prev_Max_Tension_indicator, prev_Min_Tension_indicator)
	if stress_flow > 0 --dividing by 0 protection
	then
		eta = 3 * stress_mean / stress_flow --current value of tension indicator
		lim_strain = GetInterpolatedValue(limit_strain_table, eta) --current limit strain
		dK = prev_strain_rate * dt  / lim_strain --increment Kolmogorov's criterion in step
		
		max_eta = math.max(prev_Max_Tension_indicator, eta)
		min_eta = math.min(prev_Min_Tension_indicator, eta)
		
		store(Max_Tension_indicator, max_eta)
		store(Min_Tension_indicator, min_eta)
	else
		dK = 0
		
		store(Max_Tension_indicator, prev_Max_Tension_indicator)
		store(Min_Tension_indicator, prev_Min_Tension_indicator)
	end

	K = dK + prev_Kolmogorov	

	store(Kolmogorov, K)
	store(dKolmogorov, dK)
	store(Tension_indicator, eta)
	store(Limit_strain, lim_strain)
end