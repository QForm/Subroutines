--The subroutine is intended for the Cocroft and Latham damage criterion calculation

set_target_workpiece()

Cocroft = result("Cocroft")
Cocroft_normalized = result("Cocroft_normalized")

Strain_increment = result("Strain_increment")
Stress_tensile = result("Stress_tensile")

function UserFields(stress_1, stress_flow, strain,
prev_strain, prev_Cocroft, prev_Cocroft_normalized)
	
	StrainInc = strain - prev_strain --plastic strain increment
	StressTensile = math.max(stress_1, 0) --the maximum tensile stress
	Cocroft_local = StressTensile * 1e-6 * StrainInc + prev_Cocroft --in MPa
	
	if stress_flow > 0 then --dividing by 0 protection
		NormalizingCoeff = StressTensile / stress_flow
		Cocroft_normalized_local = NormalizingCoeff * StrainInc + prev_Cocroft_normalized
	else
		Cocroft_normalized_local = prev_Cocroft_normalized
	end

	store(Cocroft, Cocroft_local)
	store(Cocroft_normalized, Cocroft_normalized_local)
	
	store(Strain_increment, StrainInc)
	store(Stress_tensile, StressTensile * 1e-6) --in MPa
end