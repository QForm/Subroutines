set_target_tool()

plasticity_threshold_allowance = math.min(math.max(parameter("Plasticity threshold allowance, %", 10) * 0.01, 0), 1)
extremal_compression_allowance = math.min(math.max(parameter("Extremal compression allowance, %", 5) * 0.01, 0), 1)

yield_stress = result("yield_stress", 0)
plasticity_status = result("plasticity_status", 0, 0, 1)
triaxiality = result("triaxiality", 0)
tension_triaxiality = result("tension_triaxiality", 0, 0, 0.55)
extremal_compression_triaxiality = result("extremal_compression_triaxiality", 0, 0, 0.55)

function UserFields(record_id, stress_eff, stress_mean, T)
  local yield_stress_new = 1510.71 - 0.52381 * T - 0.000595238 * T ^ 2
  local plasticity_status_new = math.min(math.max(plasticity_threshold_allowance > 0 and (stress_eff * 1e-6 - (1 - plasticity_threshold_allowance) * yield_stress_new) / (plasticity_threshold_allowance * yield_stress_new) or (yield_stress_new < stress_eff * 1e-6 and 1 or 0), 0), 1) + math.max((stress_eff * 1e-6 - yield_stress_new) / yield_stress_new, 0)
  local triaxiality_new = stress_mean / stress_eff
  local tension_triaxiality_new = (stress_mean > 0 and plasticity_status_new or 0) * triaxiality_new
  local extremal_compression_triaxiality_new = ((stress_mean < 0 and 1 + extremal_compression_allowance < plasticity_status_new) and plasticity_status_new or 0) * -triaxiality_new

	store(yield_stress, yield_stress_new)
	store(plasticity_status, plasticity_status_new)
	store(triaxiality, triaxiality_new)
	store(tension_triaxiality, tension_triaxiality_new)
	store(extremal_compression_triaxiality, extremal_compression_triaxiality_new)
end