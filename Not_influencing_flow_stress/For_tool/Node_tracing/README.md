# Tool's node tracing

This subroutine is designed to output a number of fields to a text file for a single node of one of the tools specified by a number corresponding to the internal numbering or coordinates of the point where it is located on the first calculation record.

- *Node_tracing.lua* is independent subroutine.
- *Node_tracing_short.lua* depends on *Utilities* module of QForm Subroutines project. 

**1. Select node**

You may specify the nude number in the `Tool's node id` parameter.
If it is -1 (default value), the subroutine will find the node that is closest to the point specified by coordinates.

**2. Set accuracy**

Set into the `Accuracy` field the count of digits that appear after decimal character of each exporting value.

**3. Select fields for exporting**

Standard fields ready for output:
- Temperature
- Mean stress and Flow stress
- Elastic strain intensity, Volumetric strain
- X, Y, Z coodrinated of node
- X, Y, Z and total displacements of node
- 1, 2, 3 principal stresses
- 1, 2, 3 principal strains

Type 1 in the input box corresponding to the field that you want to export.

**4. Add your fields**

You may add you own field to the output file. For this purpose:
	
1. Add new row into `columns` array (new field "User field" for example)

	```lua
	columns = {
		{"Record number", 1},
		...
		{"3 principal strain", export_strain_3},
		{"User field"}
	}
	```
	
2. Add new row into `possible_output` array (a nil value for example)

	```lua
	local possible_output = {
		record_id, 
		...
		strain_3,
		0
	}
	```
