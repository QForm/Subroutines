--[[
	This is the subroutine used for tool's node trasing.
	
	Enter tool's node id or set initial coordinates of tracing node.
	Set into the accuracy field the count of digits that appear after decimal character of each exporting value.
	Mark fields ready for export with 1.

	
	Author: 		Alexey Duzhev <alexeyduzhev@yandex.ru>
	Last change:	July 26, 2017
]]

set_target_tool()

tool_node_id = parameter("Tool's node id", -1)
point_start_x = parameter("Point X (mm)", 0)
point_start_y = parameter("Point Y (mm)", 0)
point_start_z = parameter("Point Z (mm)", 0)

accuracy = parameter("Accuracy", 3)

export_T = parameter("Temperature (1 - yes / 0 - no)", 1)
export_stress_mean = parameter("Mean stress (1 / 0)", 1)
export_stress_eff = parameter("Flow stress (1 / 0)", 0)
export_strain = parameter("Elastic strain intensity (1 / 0)", 0)
export_strain_vol = parameter("Volumetric strain (1 / 0)", 0)
export_x = parameter("X coordinate (1 / 0)", 0)
export_y = parameter("Y coordinate (1 / 0)", 0)
export_z = parameter("Z coordinate (1 / 0)", 0)
export_disp_x = parameter("Displacement along X (1 / 0)", 0)
export_disp_y = parameter("Displacement along Y (1 / 0)", 0)
export_disp_z = parameter("Displacement along Z (1 / 0)", 0)
export_disp_total = parameter("Total displacement (1 / 0)", 0)
export_stress_1 = parameter("1 principal stress (1 / 0)", 0)
export_stress_2 = parameter("2 principal stress (1 / 0)", 0)
export_stress_3 = parameter("3 principal stress (1 / 0)", 0)
export_strain_1 = parameter("1 principal strain (1 / 0)", 0)
export_strain_2 = parameter("2 principal strain (1 / 0)", 0)
export_strain_3 = parameter("3 principal strain (1 / 0)", 0)

point_seeker_current_distance = 1e20
point_seeker_current_node_id = -1
point_seeker_current_tool_id = -1

local function fif(test,iftrue,iffalse)	if test then return iftrue else return iffalse end end
local function tab_count(s) return string.len(s) / 4 end

head_key = 0
show_tool_id = fif(tool_node_id >= 0, 1, 0)
accuracy_shift = 10 ^ accuracy

columns = {
	{"Record number", 1},
	{"Time, s", 1},
	{"Tool id", show_tool_id}, 
	{"Temperature, °C", export_T}, 
	{"Mean stress, MPa", export_stress_mean}, 
	{"Flow stress, MPa", export_stress_eff}, 
	{"Elastic strain intensity", export_strain},
	{"Volumetric strain, 1/sic", export_strain_vol}, 
	{"X, mm", export_x}, 
	{"Y, mm", export_y}, 
	{"Z, mm", export_z}, 
	{"Displacement along X, mm", export_disp_x}, 
	{"Displacement along Y, mm", export_disp_y}, 
	{"Displacement along Z, mm", export_disp_z},
	{"Total displacement, mm", export_disp_total}, 
	{"1 principal stress, MPa", export_stress_1}, 
	{"2 principal stress, MPa", export_stress_2}, 
	{"3 principal stress, MPa", export_stress_3}, 
	{"1 principal strain", export_strain_1}, 
	{"2 principal strain", export_strain_2}, 
	{"3 principal strain", export_strain_3}
}
for i = 1, #columns do columns[i][#columns[i]+1] = 1; columns[i][2] = math.min(math.max(columns[i][2],0),1)*math.floor(tab_count(columns[i][1])+1); columns[i][3] = 0 end

function UserFields(record_id, node_id, t, T, stress_mean, stress_eff, strain, strain_vol, x, y, z, disp_x, disp_y, disp_z, stress_1, stress_2, stress_3, strain_1, strain_2, strain_3)
	
	if node_id == 0 then
		if prevRecord_id == record_id then
			tool_id = tool_id + 1
		else
			tool_id = 1
		end
	end
	prevRecord_id = record_id
	
	if tool_node_id < 0 and record_id == 1 then	
		local current_node_distance = math.sqrt((point_start_x-x*1e3)^2+(point_start_y-y*1e3)^2+(point_start_z-z*1e3)^2)
		if current_node_distance < point_seeker_current_distance then
			point_seeker_current_distance = current_node_distance
			point_seeker_current_node_id = node_id
			point_seeker_current_tool_id = tool_id
			head_key = 0
		end
	end
	
	if head_key == 0 and ((tool_id == point_seeker_current_tool_id and point_seeker_current_node_id == node_id) or tool_node_id >= 0) then
		if f~=nil then f:close() end
		f = io.open("../Node data output (" .. fif(tool_node_id < 0, point_start_x .. ", " .. point_start_y .. ", " .. point_start_z, "node " .. tool_node_id) .. ").csv", "w")
		
		local header = ""
		for i = 1, #columns do 
			if columns[i][2] > 0 then header = header .. columns[i][1] .. "\t" end
		end				
		f:write(header, "\n")
		head_key = 1
	end

	if (tool_node_id >= 0 and tool_node_id == node_id) or (tool_id == point_seeker_current_tool_id and node_id == point_seeker_current_node_id) then
	
		local output = ""
		local possible_output = {
			record_id, 
			t, 
			tool_id, 
			T, 
			stress_mean*1e-6, 
			stress_eff*1e-6, 
			strain, 
			strain_vol, 
			(x+disp_x)*1e3, 
			(y+disp_y)*1e3, 
			(z+disp_z)*1e3, 
			disp_x*1e3, 
			disp_y*1e3, 
			disp_z*1e3,
			(disp_x^2+disp_y^2+disp_z^2)^.5*1e3, 
			stress_1*1e-6, 
			stress_2*1e-6, 
			stress_3*1e-6, 
			strain_1, 
			strain_2, 
			strain_3
		}
		for i = 1, math.min(#columns, #possible_output) do
			if columns[i][2] > 0 then 
				local val_string =  tostring(math.floor(possible_output[i] * accuracy_shift + 0.5) / accuracy_shift)
				local floor_length = val_string:find("%.")
				columns[i][3] = math.max(columns[i][3], floor_length)
				val_string = string.rep(" ", columns[i][3] - floor_length) .. val_string
				local val_length = math.floor(tab_count(val_string))
				columns[i][2] = math.max(columns[i][2], val_length)
				output = output .. val_string .. string.rep("\t", columns[i][2] - val_length)
			end
		end
	
		f:write(output, "\n")
	end
end
