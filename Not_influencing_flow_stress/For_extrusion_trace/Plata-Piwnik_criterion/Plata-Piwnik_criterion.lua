set_target_extrusion_trace()

welding_chamber_z_level = parameter("Welding chamber z-level (mm)", 0)

PPC = result("PPC", 0)

function UserFields(t, dt, stress_flow, stress_mean, z, prev_p)
	local p_new = 0
	if t > 0 then
		if z * 1000 < welding_chamber_z_level then
			p_new = prev_p + math.abs(stress_mean / stress_flow) * dt
		else
			p_new = prev_p
		end
	else
		p_new = 0
	end

	store(PPC, p_new)  
end
