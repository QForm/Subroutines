set_target_extrusion_trace()

welding_chamber_z_level = parameter("Welding chamber z-level (mm)", 0)

DTC = result("DTC", 0)

function UserFields(t, dt, stress_flow, stress_mean, z, prev_d, v_x, v_y, v_z)

	local v_val = math.sqrt(v_x^2 + v_y^2 + v_z^2)
	local d_new = 0
	if t > 0 then
		if z * 1000 < welding_chamber_z_level then
			d_new = prev_d + math.abs(stress_mean / stress_flow) * dt * v_val
		else
			d_new = prev_d
		end
	end

	store(DTC, d_new)
end
