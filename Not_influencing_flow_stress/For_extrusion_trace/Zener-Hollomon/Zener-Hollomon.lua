set_target_extrusion_trace()

billet_material = parameter("Select billet material: 6005, 6060, 6061, 6063, 6082, 7005, 7075", 6060)

Zener = result("Zener")
LogZenerMax = result("LogZenerMax", -1e30)

local Q = 164800 -- activation energy
if billet_material == 6005 then
	Q = 164800
elseif billet_material == 6060 then
	Q = 144000
elseif billet_material == 6061 then
	Q = 145000
elseif billet_material == 6063 then
	Q = 141550
elseif billet_material == 6082 then
	Q = 153000
elseif billet_material == 7005 then
	Q = 147900
elseif billet_material == 7075 then
	Q = 129400
end

R = 8.314 -- gas constant

function UserFields (T, prev_strain_rate, strain_rate, prev_LogZenerMax)	
	T = T + 273.15  				  		-- conversion temperature in Kelvin	
	z = strain_rate*math.exp(Q/(R*T)) 		-- calculation of the Zener-Hollomon criterion
	lz = math.log(z)				  		-- natural logarithm. math.log(z, 10) – logarithm	
	lz_max = math.max(prev_LogZenerMax, lz) -- save the maximum value of prev_LogZenerMax and lzprev_LogZenerMax и lz
	
	store(Zener, lz)						-- sent values to QForm
	store(LogZenerMax, lz_max)
end