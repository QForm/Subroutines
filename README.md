# QForm user subroutines
User subroutines are intended for computing user-defined fields that initially are not available in the list of fields of [QForm](http://qform3d.com) program as well as for enhancing the capabilities of defining flow stresses in a workpiece. The following types of user-defined subroutines are possible:

1. **Influencing flow stress** (loaded in Deformed materials database)
 - dependent only on standard fields
 - dependent on standard fields and used-defined fields
2. **Not influencing flow stress** (loaded in Sourse data control panel) 

User subroutines in [QForm](http://qform3d.com) are written in Lua language (refer to www.lua.org, www.lua.ru, [wikipedia.org/wiki/Lua_(programming_language)](http://wikipedia.org/wiki/Lua_(programming_language))) designed for creation of built-in applications.

A subroutine code in the Lua language may be written using a standard text editor (Notepad), although it is advisable to use [Notepad++](http://notepad-plus-plus.org) or [ZeroBrane Studio](https://studio.zerobrane.com). The file extension of the text document should be changed to \*.lua. The name and location of this file not importance.

As per the Lua terminology — [QForm](http://qform3d.com) is a host program that includes a so-called JIT-compiler (just-in-time). User subroutines have access to all standard fields and variables in the [QForm](http://qform3d.com) program. Fields are defined in each node of the finite-element mesh of tools and a workpiece. Note that names of variables in the Lua language are not always the same as names of fields in the [QForm](http://qform3d.com) interface, and they are measured in SI (metric) units.

All fields in the Lua language are accessible in two variations: values in the current step (for example, a value of the temperature in the current node – `T`) and in the previous step with the prefix `prev_` (for example, a value of the temperature in the current node – `prev_T`). The following variables have no previous values: `t`, `dt`.