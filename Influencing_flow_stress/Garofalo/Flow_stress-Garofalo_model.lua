--Values of coefficients for aluminum alloy AA6005 for Kelvin
A  = parameter("A (s)", 4.66539e+11)
Q = parameter("Q in J/mol", 181021) 
n = parameter("n", 4.51)
R = parameter("R in ", 8.314)
Alpha = parameter("Alpha (1/MPa)", 0.045)*1e-6
T_min = parameter("T min (C)", 20) + 273.15
T_max = parameter("T max (C)", 600) + 273.15
strain_rate_min = parameter("Strain rate min", 0.001)


-- Lua 5.3 don't have the trigonometric hyperbolic functions in the standard math library
function arcsinh(x)
  return math.log(x + (x ^ 2 + 1) ^ 0.5)
end

function FlowStress(strain_rate, T)
	strain_rate = math.max(strain_rate, strain_rate_min)	
	T = math.min(math.max(T + 273.15, T_min), T_max)
	
	return (1 / Alpha) * arcsinh(((1 / A) * strain_rate * (math.exp(Q / (R * T))))^(1 / n))
end