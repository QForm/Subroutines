--The subroutine is intended for setting of the flow stress by the Hensel-Spittel formula
--Have to be imported in workpiece material database

--Values of coefficients for aluminum alloy AA6060
A  = parameter("A in MPA", 656)*1e6
m1 = parameter("m1", -0.006840179)
m2 = parameter("m2",  0.030279794)
m3 = parameter("m3", -0.023364665)
m4 = parameter("m4", -0.004523879)
m5 = parameter("m5",  0.0000886057)
m7 = parameter("m7", -0.084881332)
m8 = parameter("m8",  0.000364076)
m9 = parameter("m9", 0)
 
--Override the standard functions for short entries
exp = math.exp
 
function FlowStress (T, strain, strain_rate)
    F = A * exp(m1 * T + m4 / strain + m7 * strain) * strain ^ m2 * (1 + strain) ^ (m5 * T) * strain_rate ^ (m3 + m8 * T) * T ^ m9
    return F
end