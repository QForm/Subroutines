Processing routines are somewhat different from *postprocessing* subroutines. First, such a subroutine performs computation in the process of simulation that affects the ultimate result. Secondly, there are some differences in its structure.

For using of a subroutine for calculation of flow stress it is needed to create the workpiece material in the **Deformed materials** database or copy an existing one, select **Subroutine** in **Varible type** of **Flow stress** tab and press **Load from file** button. After selection of a file with the Lua code, a table with parameters will appear if they have been set in the subroutine. Upon saving of the material it may be assigned to a forged part and the simulation may be started.

![QForm interface for processing subroutines](interface.png)

The subroutine can contain function `UserFields()` and must contain function `FlowStress()`. 
The function `FlowStress()` accepts standard and user-defined fields (from previous simulation step) in a forged part. To return the flow stress value to [QForm](http://qform3d.com) command `return {varible_name}` into `FlowStress()` function must be used.