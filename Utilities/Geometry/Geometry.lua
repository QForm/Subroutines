utilities = utilities or {}

Point = {x = 0, y = 0, z = 0, vector = nil}
function Point:new(_x, _y, _z)
	local obj = {}
	obj.x = _x or 0
	obj.y = _y or 0
	obj.z = _z or 0
	obj.vector = Vector:new(obj.x, obj.y, obj.z)
	
	function obj:get_distance(pointB)
		return ((pointB.x - self.x) ^ 2 + (pointB.y - self.y) ^ 2 + (pointB.z - self.z) ^ 2) ^ .5
	end
  
  function obj:plus(vecB)
    return Point:new(self.x + vecB.x, self.y + vecB.y, self.z + vecB.z)
  end
  
  function obj:minus(vecB)
    return Point:new(self.x - vecB.x, self.y - vecB.y, self.z - vecB.z)
  end
	
	setmetatable(obj, self)
	self.__index = self
	return obj
end

Vector = {x = 0, y = 0, z = 0, magnitude = 0}
function Vector:new(_x, _y, _z)
	local obj = {}
	obj.x = _x or 0
	obj.y = _y or 0
	obj.z = _z or 0
	obj.magnitude = (obj.x ^ 2 + obj.y ^ 2 + obj.z ^ 2) ^ .5
  
  function obj:point()
    return Point:new(self.x, self.y, self.z)
  end
  
  function obj:plus(vecB)
    return Vector:new(self.x + vecB.x, self.y + vecB.y, self.z + vecB.z)
  end
  
  function obj:minus(vecB)
    return Vector:new(self.x - vecB.x, self.y - vecB.y, self.z - vecB.z)
  end
  
  function obj:scale(factor)
    return Vector:new(self.x * factor, self.y * factor, self.z * factor)
  end
	
	function obj:normalize()
		return self.magnitude > 0 and self:scale(1 / self.magnitude) or self
	end
	
	function obj:dot(vecB)
		return self.x * vecB.x + self.y * vecB.y + self.z * vecB.z
	end
	
	function obj:cross(vecB)
		return Vector:new(self.y * vecB.z - self.z * vecB.y, self.z * vecB.x - self.x * vecB.z, self.x * vecB.y - self.y * vecB.x)
	end
	
	function obj:arbitrary_perpendicular()
		if self.magnitude == 0 then return self end
		
		if self.y ~= 0 or self.z ~= 0 then
			return self:cross(Vector:new(1, 0, 0))
		else
			return self:cross(Vector:new(0, 1, 0))
		end
	end
  
  function obj:is_parallel(vecB)
    return vecB ~= nil and self:cross(vecB).magnitude == 0
  end
  
  function obj:rotate(_rotationMatrix)
--    local A = (self.x*_rotationMatrix[1]+self.y*_rotationMatrix[2]+self.z*_rotationMatrix[3])
--    local B = (self.x*_rotationMatrix[4]+self.y*_rotationMatrix[5]+self.z*_rotationMatrix[6])
--    local C = (self.x*_rotationMatrix[7]+self.y*_rotationMatrix[8]+self.z*_rotationMatrix[9])
--    return Vector:new(
--      _rotationMatrix[1] * A + _rotationMatrix[2] * B + _rotationMatrix[3] * C,
--      _rotationMatrix[4] * A + _rotationMatrix[5] * B + _rotationMatrix[6] * C,
--      _rotationMatrix[7] * A + _rotationMatrix[8] * B + _rotationMatrix[9] * C)
--    return Vector:new(
--        self.x*_rotationMatrix[1]+self.y*_rotationMatrix[4]+self.z*_rotationMatrix[7],
--        self.x*_rotationMatrix[2]+self.y*_rotationMatrix[5]+self.z*_rotationMatrix[8],
--        self.x*_rotationMatrix[3]+self.y*_rotationMatrix[6]+self.z*_rotationMatrix[9])
      return Vector:new(
          self.x*_rotationMatrix[1]+self.y*_rotationMatrix[2]+self.z*_rotationMatrix[3],
          self.x*_rotationMatrix[4]+self.y*_rotationMatrix[5]+self.z*_rotationMatrix[6],
          self.x*_rotationMatrix[7]+self.y*_rotationMatrix[8]+self.z*_rotationMatrix[9])
  end
	
	setmetatable(obj, self)
	self.__index = self
	return obj
end
function Vector:new_points(_pntA, _pntB)
	_pntA = _pntA or Point:new(0, 0, 0)
	_pntB = _pntB or Point:new(0, 0, 0)
	return Vector:new(_pntB.x - _pntA.x, _pntB.y - _pntA.y, _pntB.z - _pntA.z)
end

Axis = {origin = Point:new(0, 0, 0), direction = Vector:new(0, 0, 0)}
function Axis:new(_origin, _direction)
	local obj = {}
	obj.origin = _origin or Point:new(0, 0, 0)
	obj.direction = _direction:normalize() or Vector:new(0, 0, 0)
	
	function obj:project_point(pnt)
		local p01 = self.direction.x * (self.origin.x - pnt.x) + self.direction.y * (self.origin.y - pnt.y) + self.direction.z * (self.origin.z - pnt.z)

		return Point:new(
			self.origin.x - p01 * self.direction.x,
			self.origin.y - p01 * self.direction.y,
			self.origin.z - p01 * self.direction.z
		)
	end
	
	setmetatable(obj, self)
	self.__index = self
	return obj
end

function rotationMatrix(_direction,_theta)
  local nDir = _direction:normalize()
  local cosTheta = math.cos(_theta)
  local dcosTheta = 1 - cosTheta
  local sinTheta = math.sin(_theta)
  return {
    cosTheta + dcosTheta * nDir.x * nDir.x,
    dcosTheta * nDir.x * nDir.y-sinTheta * nDir.z,
    dcosTheta * nDir.x * nDir.z + sinTheta * nDir.y, 
    dcosTheta * nDir.y * nDir.x + sinTheta * nDir.z,
    cosTheta + dcosTheta * nDir.y * nDir.y,
    dcosTheta * nDir.y * nDir.z-sinTheta * nDir.x, 
    dcosTheta * nDir.z * nDir.x-sinTheta * nDir.y,
    dcosTheta * nDir.z * nDir.y + sinTheta * nDir.x,
    cosTheta + dcosTheta * nDir.z * nDir.z}
end

function tensorRotate(_components,_rotationMatrix)
  return {
    (_rotationMatrix[1])^2*_components[1]+2*_rotationMatrix[1]*(_rotationMatrix[2]*_components[4]+_rotationMatrix[3]*_components[5])
    +(_rotationMatrix[2])^2*_components[2]+2*_rotationMatrix[2]*_rotationMatrix[3]*_components[6]+(_rotationMatrix[3])^2*_components[3],
    (_rotationMatrix[4])^2*_components[1]+2*_rotationMatrix[4]*(_rotationMatrix[5]*_components[4]+_rotationMatrix[6]*_components[5])
    +(_rotationMatrix[5])^2*_components[2]+2*_rotationMatrix[5]*_rotationMatrix[6]*_components[6]+(_rotationMatrix[6])^2*_components[3],
    (_rotationMatrix[7])^2*_components[1]+2*_rotationMatrix[7]*(_rotationMatrix[8]*_components[4]+_rotationMatrix[9]*_components[5])
    +(_rotationMatrix[8])^2*_components[2]+2*_rotationMatrix[8]*_rotationMatrix[9]*_components[6]+(_rotationMatrix[9])^2*_components[3],
    _rotationMatrix[1]*(_rotationMatrix[4]*_components[1]+_rotationMatrix[5]*_components[4]+_rotationMatrix[6]*_components[5])+
    _rotationMatrix[2]*(_rotationMatrix[4]*_components[4]+_rotationMatrix[5]*_components[2]+_rotationMatrix[6]*_components[6])+
    _rotationMatrix[3]*(_rotationMatrix[4]*_components[5]+_rotationMatrix[5]*_components[6]+_rotationMatrix[6]*_components[6]),
    _rotationMatrix[1]*(_rotationMatrix[7]*_components[1]+_rotationMatrix[8]*_components[4]+_rotationMatrix[9]*_components[5])+
    _rotationMatrix[2]*(_rotationMatrix[7]*_components[4]+_rotationMatrix[8]*_components[2]+_rotationMatrix[9]*_components[6])+
    _rotationMatrix[3]*(_rotationMatrix[7]*_components[5]+_rotationMatrix[8]*_components[6]+_rotationMatrix[9]*_components[6]),
    _rotationMatrix[4]*(_rotationMatrix[7]*_components[1]+_rotationMatrix[8]*_components[4]+_rotationMatrix[9]*_components[5])+
    _rotationMatrix[5]*(_rotationMatrix[7]*_components[4]+_rotationMatrix[8]*_components[2]+_rotationMatrix[9]*_components[6])+
    _rotationMatrix[6]*(_rotationMatrix[7]*_components[5]+_rotationMatrix[8]*_components[6]+_rotationMatrix[9]*_components[6])
  }
end

return utilities
