# Geometry

## Point

A 3D position

### Properties

| **Name** | **Description**                                |
| -------- | ---------------------------------------------- |
| x        | X coordinate of point                          |
| y        | Y coordinate of point                          |
| z        | Z coordinate of point                          |
| vector   | Gets the vector from the origin to this point  |

### Constructors

| **Name**     | **Description**                |
| ------------ | ------------------------------ |
| new(x, y)    | Creates point by 2 coordinates |
| new(x, y, z) | Creates point by 3 coordinates |

### Methods

| **Name**            | **Description**                                         |
| ------------------- | ------------------------------------------------------- |
| get_distance(point) | Get distance between this and specified point           |
| plus(vector)          | Returns new point moved by specified vector           |
| minus(vector)          | Returns new point moved by inversed specified vector |

### Usage

```lua
local pntA = Point:new(0, 0, 0)
local pntB = Point:new(1, 1, 1)
pntA:get_distance(pntB)
>    1.7320508075689
```


## Vector

A 3D displacement vector

### Properties

| **Name**  | **Description**                  |
| --------- | -------------------------------- |
| x         | X projection of vector           |
| y         | Y projection of vector           |
| z         | Z projection of vector           |
| magnitude | Gets the magnitude of the vector |
| normalize | Gets the unit vector             |

### Constructors

| **Name**                   | **Description**                      |
| -------------------------- | ------------------------------------ |
| new(x, y)                  | Creates vector by 2 coordinates      |
| new(x, y, z)               | Creates vector by 3 coordinates      |
| new_points(pointA, pointB) | Creates vector from pointA to pointB |

### Methods

| **Name**                  | **Description**                                    |
| ------------------------- | -------------------------------------------------- |
| point()                   | Returns the point on the end of vector             |
| plus(vector)              | Returns new vector as sum of two vectors           |
| minus(vector)             | Returns new vector as subtration of two vectors    |
| scale(factor)             | Magnifies vector by factor times                   |
| dot(vector)               | Calculates the dot product of two vectors          |
| cross(vector)             | Calculates the cross product of two vectors        |
| arbitrary_perpendicular() | Gets an aribitrary perpendicular direction         |
| is_parallel()             | Checks if two vectors are parallel                 |
| rotate(matrix)            | Returns vector transformed using rotation matrix   |

### Usage

```lua
local vecA = Vector:new(1, 2, 3)
local vecB = Vector:new(4, 5, 6)
vecA:dot(vecB)
>    32.0
vecA:cross(vecB)
>    Vector[-3, 6, -3]
vecA:arbitrary_perpendicular()
>    Vector[0, 3, -2]
vecA:isParallel(vecB)()
>    False
```


## Axis

A pair of origin point and direction

### Properties

| **Name**   | **Description**              |
| ---------- | ---------------------------- |
| origin     | A 3D point                   |
| direction  | Normalized direction vector  |

### Constructors

| **Name**           | **Description**                                       |
| ------------------ | ----------------------------------------------------- |
| new(point, vector) | Creates axis by origin point and vector of any length |

### Methods

| **Name**             | **Description**                        |
| -------------------- | -------------------------------------- |
| project_point(point) | Returns new point projected onto axis  |

### Usage

```lua
local origin = Point:new(1, 1, 1)
local direction = Vector:new(1, 2, 3)
local axis = Axis:new(origin, direction)
local point = Point:new(10, 10, 10)
axis:project_point(point)
>    Point[4.8571428571429, 8.7142857142857, 12.571428571429]
```

## Other Functions

### RotationMatrix

Creating linear array that is used to perform a rotation in Euclidean space.

| **Variables**        | **Description**                        |
| -------------------- | -------------------------------------- |
| direction            | vector to rotate around                |
| theta                | angle value (in radians)               |

### Usage

```lua
local rotationVector = Vector:new(0, 0, 1)
local rotationAngle = math.pi/6
rotationMatrix(rotationVector, rotationAngle)
>    {0.86602540378443871, -0.5, 0, 0.5, 0.86602540378443871, 0, 0, 0, 1}
```

### tensorRotate

Rotating tensor according to rotation matrix

| **Variables**        | **Description**                                            |
| -------------------- | ---------------------------------------------------------- |
| components           | target tensor (linear array: T₁₁, T₂₂, T₃₃, T₁₂, T₁₃, T₂₃) |
| rotationMatrix       | matrix of directional cosines (linear array)               |

### Usage

```lua
local rotationVector = Vector:new(0, 0, 1)
local rotationAngle = math.pi/6
local rotMatrix = rotationMatrix(rotationVector, rotationAngle)
local tensor = {-100, 100, 100, 0, 0, 50}
tensorRotate(tensor, rotMatrix)
>    {-50.000000000000021, 50.000000000000021, 100, -86.602540378443862, -24.999999999999996, 43.301270189221938}
```
