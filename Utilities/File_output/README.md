# utilities.output_table Function

Provides space-formatted data output from table into the specified file.

```lua
utilities.output_table(data, filename, accuracy)
utilities.output_table(data, filename)
```

### Usage

**Numerical table**

You can save sctructured numerical data. Function creates constant-width columns separates by space characters.

```lua
-- Connect Utilities module
require "Utilities"

-- Generate sine value pairs
local data = {{"x", "sin(x)"}}
for x = 0, 2*math.pi, 0.2*math.pi do
	data[#data+1] = {x, math.sin(x)}
end

-- Create new file
utilities.output_table(data, "sine.csv")
```

```
>      x       sin(x)   
>      0.0      0.0     
>      0.6283   0.5878  
>      1.2566   0.9511  
>      1.885    0.9511  
>      2.5133   0.5878  
>      3.1416   0.0     
>      3.7699  -0.5878  
>      4.3982  -0.9511  
>      5.0265  -0.9511  
>      5.6549  -0.5878  
>      6.2832   0.0         
```

**Textual output**

Function also works with textual data creating constant-width columns for specified table.

```lua
require "Utilities"

local data = {
	{"Steel Type", "Maximum forging temperature (°F / °C)", "Burning temperature (°F / °C)"},
	{"1.5% carbon", "1920 / 1049", "2080 / 1140"},
	{"1.1% carbon", "1980 / 1082", "2140 / 1171"},
	{"0.9% carbon", "2050 / 1121", "2230 / 1221"},
	{"0.5% carbon", "2280 / 1249", "2460 / 1349"},
	{"0.2% carbon", "2410 / 1321", "2680 / 1471"},
	{"3.0% nickel steel", "2280 / 1249", "2500 / 1371"},
	{"3.0% nickel–chromium steel", "2280 / 1249", "2500 / 1371"},
	{"5.0% nickel (case-hardening) steel", "2320 / 1271", "2640 / 1449"},
	{"Chromium–vanadium steel", "2280 / 1249", "2460 / 1349"},
	{"High-speed steel", "2370 / 1299", "2520 / 1385"},
	{"Stainless steel", "2340 / 1282", "2520 / 1385"},
	{"Austenitic chromium–nickel steel", "2370 / 1299", "2590 / 1420"},
	{"Silico-manganese spring steel", "2280 / 1249", "2460 / 1350"}
}

utilities.output_table(data, "Forging temperatures.txt")
```

```
>      Steel Type                          Maximum forging temperature (°F / °C)  Burning temperature (°F / °C)
>      1.5% carbon                         1920 / 1049                            2080 / 1140                    
>      1.1% carbon                         1980 / 1082                            2140 / 1171                    
>      0.9% carbon                         2050 / 1121                            2230 / 1221                    
>      0.5% carbon                         2280 / 1249                            2460 / 1349                    
>      0.2% carbon                         2410 / 1321                            2680 / 1471                    
>      3.0% nickel steel                   2280 / 1249                            2500 / 1371                    
>      3.0% nickel–chromium steel          2280 / 1249                            2500 / 1371                    
>      5.0% nickel (case-hardening) steel  2320 / 1271                            2640 / 1449                    
>      Chromium–vanadium steel             2280 / 1249                            2460 / 1349                    
>      High-speed steel                    2370 / 1299                            2520 / 1385                    
>      Stainless steel                     2340 / 1282                            2520 / 1385                    
>      Austenitic chromium–nickel steel    2370 / 1299                            2590 / 1420                    
>      Silico-manganese spring steel       2280 / 1249                            2460 / 1350                    
```