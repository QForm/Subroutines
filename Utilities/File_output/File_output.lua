utilities = utilities or {}

function utilities.output_table(data, filename, accuracy)	
	accuracy = accuracy or 4
	local accuracy_shift = 10 ^ accuracy
	
	if type(data)~="table" then data = {data} end
	local new_data = {}
	
	local columns_width = {}
	for i = 1, #data do
		if type(data[i])~="table" then data[i] = {data[i]} end
		new_data[i] = {}
		for k = 1, #data[i] do
			if #columns_width < k then columns_width[k] = {0,0} end
			if type(data[i][k]) == "number" then
				local string_value = tostring(math.floor(data[i][k] * accuracy_shift + 0.5) / accuracy_shift)
				new_data[i][k] = {string_value, string_value:find("%."), string_value:len()}
				columns_width[k][1] = math.max(new_data[i][k][2], columns_width[k][1])
				columns_width[k][2] = math.max(new_data[i][k][3] - columns_width[k][1], columns_width[k][2])
			elseif type(data[i][k]) == "string" then
				new_data[i][k] = {data[i][k], math.huge, data[i][k]:len()}
				columns_width[k][2] = math.max(new_data[i][k][3] - columns_width[k][1], columns_width[k][2])
			else
				local string_value = tostring(data[i][k])
				new_data[i][k] = {string_value, math.huge, string_value:len()}
				columns_width[k][2] = math.max(new_data[i][k][3] - columns_width[k][1], columns_width[k][2])
			end
		end
	end
	
	local output = ""
	for i = 1, #new_data do
		if i > 1 then output = output .. "\n" end
		for k = 1, #new_data[i] do
			if k > 1 then output = output .. "  " end
			local add_string = string.rep(" ", columns_width[k][1] - math.min(new_data[i][k][2], columns_width[k][1])) .. new_data[i][k][1]
			output = output .. add_string .. string.rep(" ", columns_width[k][2] + columns_width[k][1] - add_string:len())
		end
	end
	
	f = io.open(filename, "w")
	f:write(output)
	f:close()
end

return utilities
