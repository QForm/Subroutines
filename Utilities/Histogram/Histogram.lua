--Collect data for histogram and save it to Histogram_data.txt into project data folder.
--The record (step) of calculation must be set. Histogram cannot be built for the last step of operation (blow).
--Bins (intervals) quantities must be set.
--If set min/max values are equal, these values takes from the simulation data.

set_target_workpiece()

max_value = parameter("max_value", 0)
min_value = parameter("min_value", 0)
record = parameter("record", 1)
bins_number =  parameter("bins_number", 5)

function UserFields(T, record_id, node_id, volume)
	Histogram (T, bins_number, record, min_value, max_value, record_id, node_id, volume)
end

function Histogram(Field, bins_number, record, min_value, max_value, record_id, node_id, volume)
	if f == nil and bins_number > 0
	then
		f = io.open("Histogram_data.txt", "w")
	end
	if data == nil
	then
		data = {}
	end
	
	if record_id == record  and bins_number > 0
	then
		table.insert(data, {Field, volume}) --fill data array
	end
	
	if record_id == record + 1 and node_id == 0  and bins_number > 0
	then
		local out_string = 'Interval' .. '\t' .. 'Quantity, %' .. '\n'
		local bin_value = 0
		local full_volume = 0
		local real_max_value = -1e40
		local real_min_value = 1e40

		for i = 1, #data, 1 do --find min/max of the field
			real_min_value = math.min(data[i][1], real_min_value)
			real_max_value = math.max(data[i][1], real_max_value)
			
			full_volume = full_volume + data[i][2] * 1e9 --full volume
		end

		bins_values = {} --defined intervals
		if max_value == min_value
		then
			bin_width = (real_max_value - real_min_value) / bins_number
			for i = 1, bins_number, 1 do
				bin_min = real_min_value + bin_width * (i - 1)
				bin_max = real_min_value + bin_width * i
				table.insert(bins_values, {bin_min, bin_max, bin_value})
			end
		else
			if max_value < min_value
			then
				local temp = max_value
				max_value = min_value
				min_value = temp
			end
			
			bin_width = (max_value - min_value) / bins_number
			for i = 1, bins_number, 1 do
				bin_min = min_value + bin_width * (i - 1)
				bin_max = min_value + bin_width * i
				table.insert(bins_values, {bin_min, bin_max, bin_value})
			end
			
			if min_value > real_min_value
			then
				bin_min = nil
				bin_max = min_value
				table.insert(bins_values, 1, {bin_min, bin_max, bin_value})
			end
			
			if max_value < real_max_value
			then
				bin_min = max_value
				bin_max = nil
				table.insert(bins_values, {bin_min, bin_max, bin_value})
			end
		end
		
		for i = 1, #data, 1 do --divide values into intervals
			for k = 1, #bins_values, 1 do
				if  bins_values[k][1] == nil
				then
					if data[i][1] < bins_values[k][2]
					then
						bins_values[k][3] = bins_values[k][3] + data[i][2] * 1e9
					end
				elseif bins_values[k][2] == nil
				then
					if bins_values[k][1] < data[i][1]
					then
						bins_values[k][3] = bins_values[k][3] + data[i][2] * 1e9
					end
				elseif bins_values[k][1] <= data[i][1] and data[i][1] < bins_values[k][2]
				then
					bins_values[k][3] = bins_values[k][3] + data[i][2] * 1e9
				elseif k == #bins_values and bins_values[k][2] ~= nil and bins_values[k][1] <= data[i][1] and data[i][1] <= bins_values[k][2]
				then
					bins_values[k][3] = bins_values[k][3] + data[i][2] * 1e9
				end
			
			end
		end
		
		for i = 1, #bins_values, 1 do --form a string of output data
			if bins_values[i][1] == nil
			then
				out_string = out_string .. '< ' .. bins_values[i][2] .. '\t' .. bins_values[i][3] / full_volume * 100 .. '\n'
			elseif bins_values[i][2] == nil
			then
				out_string = out_string .. bins_values[i][1] .. ' <' .. '\t' .. bins_values[i][3] / full_volume * 100 .. '\n'
			else
				out_string = out_string .. bins_values[i][1] .. '-' .. bins_values[i][2] .. '\t' .. bins_values[i][3] / full_volume * 100 .. '\n'
			end
		end
		
		f:write (out_string .. '\n') --output data to the file
		f:write ('Max value' .. '\t' .. real_max_value, '\n')
		f:write ('Min value' .. '\t' .. real_min_value, '\n')
	end
end