# Utilities module

This folder contains an Utilities module of **QForm Subroutines** project. Here you can find assist subroutines that can make export/import data from simulation results and other subroutines that help to make subroutines coding more convenient.

How to connect Utilities module to your script?

## 1. Specify path to the folder

Lua engine have to know where are the module files. To help it to find them you may:

**- Place directory contents into one of the inner directories of QForm:**

	C:\QForm\8.2.0\x64\lua\
	C:\QForm\8.2.0\x64\

**- Set the environment variable `LUA_PATH`**

```
set LUA_PATH=;;C:\lua\Utilities\?.lua
```

Please note that the semi-colon is used to separate patterns. The double semicolon at the start means "append this to the existing Lua module path" so Lua will first look in the system-wide directory for modules.

**- Modify `package.path`**

Modify standard variable `package.path` at the beginning of your script file.

```lua
package.path = "C:/lua/Utilities/?.lua;" .. package.path
```
	

## 2. Connect module to script

```lua
require "Utilities"
```
Place `require` command at the beginning of the script file. The module will be connected and all functions'll be available.

Also you can connect explicit part of Utilities module. For example to use *File_output* functions append following code to your script:
```lua
require "File_output.File_output"
```
