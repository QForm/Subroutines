# Additional.lua

The module contains additional functions for frequently used operations

## Module contents

 * utilities
    * [fif](#utilitiesfif-function)
 * table
    * [clone](#tableclone-function)
    * [count](#tablecount-function)
    * [has_value](#tablehas_value-function)
    * [max](#tablemax-function)
    * [mean](#tablemean-function)
    * [median](#tablemedian-function)
    * [min](#tablemin-function)
    * [quantile](#tablequantile-function)
    * [reverse](#tablereverse-function)
    * [total](#tabletotal-function)
    * [trimmed_mean](#tabletrimmed_mean-function)
	* [interpolate](#tableinterpolate-function) 
 * math
    * [atan2](#mathatan2-function)
    * [bintodec](#mathbintodec-function)
    * [dectobin](#mathdectobin-function)
    * [dectohex](#mathdectohex-function)
    * [minmax](#mathminmax-function)
    * [round](#mathround-function)
    * [sign](#mathsign-function)
	* [mod] (#mathmod-function)

## utilities.fif Function

Functional **If** operator

```lua
utilities.fif(test, iftrue, iffalse)
```

### Usage

```lua
for i = 1, 10 do
	print(utilities.fif(math.log(i) / math.log(2) % 1 == 0, true, i))
end
```
```
>    true
>    true
>    3
>    true
>    5
>    6
>    7
>    true
>    9
>    10
```

Consider that all variables are not fault. For example,
```lua
fif(1+2==3, 1, a[0])
```
```
>  1: attempt to index a nil value (global 'a')
```
will throw exception in `a` has nil value.  
In such cases you may use following construction:
```lua
1+2==3 and 1 or a[0]
```
```
>  1
```


## table.clone Function

Returns an independent copy of table

```lua
table.clone(input_table)
```

### Usage

```lua
a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
b = a
c = table.clone(a)
a[4] = 0
```
```
a: {1, 2, 3, 0, 5, 6, 7, 8, 9, 10}
b: {1, 2, 3, 0, 5, 6, 7, 8, 9, 10}
c: {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
```


## table.count Function

Returns the number of elements in table

```lua
table.count(table)
```

### Usage

```lua
table.count({13, 94, 4, 88, 54, 50, 92, 10, 61, 40})
>	10
```


## table.has_value Function

Returns first position of specifiend element in the table or *-1*.

```lua
table.has_value(table, value)
```

### Usage

```lua
a = {}
for i = 1, 10 do a[i] = 2 ^ i end
table.has_value(a, 4)
>   2
table.has_value(a, 7)
>   -1
```


## table.max Function

Returns the maximal value in table

```lua
table.max(table)
```

### Usage

```lua
table.max({13, 94, 4, 88, 54, 50, 92, 10, 61, 40})
>	92
```


## table.mean Function

Returns the statistical mean of the elements in table

```lua
table.mean(table)
```

### Usage

```lua
table.mean({13, 94, 4, 88, 54, 50, 92, 10, 61, 40})
>	50.6
```


## table.median Function

Returns the median of the elements in table

```lua
table.median(table)
```

### Usage

```lua
table.median({13, 94, 4, 88, 54, 50, 92, 10, 61, 40})
>	52.0
```


## table.min Function

Returns the minimal value in table

```lua
table.min(table)
```

### Usage

```lua
table.min({13, 94, 4, 88, 54, 50, 92, 10, 61, 40})
>	4
```


## table.quantile Function

Returns the qᵗʰ quantile of table

```lua
table.quantile(table, q)
```

### Usage

```lua
table.quantile({13, 94, 4, 88, 54, 50, 92, 10, 61, 40}, .5)
>	50
table.quantile({13, 94, 4, 88, 54, 50, 92, 10, 61, 40}, .25)
>	13
```


## table.reverse Function

Returns as reversed copy of table

```lua
table.reverse(input_table)
```

### Usage

```lua
a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10}
b = table.reverse(a)
```
```
a: {1, 2, 3, 0, 5, 6, 7, 8, 9, 10}
b: {10, 9, 8, 7, 6, 5, 4, 3, 2, 1}
```


## table.total Function

Returns the total of the elements in table

```lua
table.total(table)
```

### Usage

```lua
table.total({13, 94, 4, 88, 54, 50, 92, 10, 61, 40})
>	506
```


## table.trimmed_mean Function

Returns the mean of the elements in table after dropping a fraction of the smallect and largest elements

```lua
table.trimmed_mean(table, f1, f2)
table.trimmed_mean(table, f) == table.mean(table, f1, f2)
table.trimmed_mean(table) == table.mean(table, 0.05)
```

### Usage

```lua
table.mean({-10, 1, 1, 1, 1, 20}, .2)
>	2.333333333
table.trimmed_mean({-10, 1, 1, 1, 1, 20}, .2)
>	1.0
table.trimmed_mean({-10, 1, 1, 1, 1, 20}, .2, 0)
>	4.8
table.mean({-10, 1, 1, 1, 1, 20})
>	2.333333333
```

## table.interpolate Function

Returns interpolated value

| **Variables**        | **Description**                                                  |
| -------------------- | ---------------------------------------------------------------- |
| knots                | array of knots (pair of {coordinate, value}) that must be sorted |
| value                | coordinate to interpolate                                        |

### Usage

```lua
local knots = {{0, 1},{1, 0}}
local value = 0.5
table.interpolate(knots, value)
>	0.5

## math.atan2 Function

Returns the arctangent (in radians) of the quotient of its arguments.

```lua
math.atan2(x, y)
```


## math.bintodec Function

Converts specified binary number to decimal

```lua
math.bintodec(str)
math.bintodec(str, split_count)
```

### Usage

```lua
math.bintodec("11011010")
>	218.0
math.bintodec("11011010",10)
>	{218.0}
math.bintodec("11011010",4)
>	{13.0, 10.0}
math.bintodec("11011010",3)
>	{3.0, 3.0, 2.0}
```


## math.dectobin Function

Converts specified decimal number to binary

```lua
math.dectobin(x)
math.dectobin(x, count)
```

### Usage

```lua
math.dectobin(1)
>	1
math.dectobin(2)
>	10
math.dectobin(10)
>	1010
math.dectobin(30)
>	100101100
math.dectobin(300, 5)
>	01100
math.dectobin(300, 20)
>	00000000000100101100
```


## math.dectohex Function

Converts specified decimal number to hexadecimal

```lua
math.dectohex(x)
math.dectohex(x, count)
```

### Usage

```lua
math.dectohex(1)
>	1
math.dectohex(2)
>	2
math.dectohex(10)
>	a
math.dectohex(12345)
>	3039
math.dectohex(12345, 2)
>	39
math.dectohex(12345, 20)
>	00000000000000003039
```


## math.minmax Function

Returns either specified number if it lies in the *[x_min, x_max]* interval or the nearest bound.

```lua
math.minmax(x, x_min, x_max)
```

### Usage

```lua
for i = 1, 10 do
	print(math.minmax(i, 3, 7)
end
```
```
>    3
>    3
>    3
>    4
>    5
>    6
>    7
>    7
>    7
>    7
```


## math.round Function

Returns the value of a number rounded to the nearest integer.

```lua
math.round(x)
```


## math.sign Function

Returns the sign of the specified number. The value is either *-1*, *1* or *0*.

```lua
math.sign(x)
```

## math.mod Function

Divides two numbers and returns only the remainder.

```lua
math.mod(x, y)
```

### Usage

```lua
math.mod(10, 5)
>	0
math.mod(10, 3)
>	1
math.mod(-10, 3)
>	-1
math.mod(12, 4.3)
>	3.4
math.mod(12.6, 5)
>	2.6
math.mod(47.9, 9.35)
>	1.15
```