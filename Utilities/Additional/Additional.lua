utilities = utilities or {}

--[[
	Functional If
	
	Can be used in single-line expressions.
	
	Consider that all variables are not fault. For example,
		fif(1+2==3, 1, a[0])
	will throw exception if 'a' has nil value.
	In such cases you may use following construction:
		1+2==3 and 1 or a[0]
]]
function utilities.fif(test, iftrue, iffalse)
	if test then 
		return iftrue 
	else 
		return iffalse 
	end
end



function math.sign(x)
	if x == 0 then
		return 0
	elseif x > 0 then
		return 1
	else
		return -1
	end
end

function math.round(x)
	return math.floor(x + 0.5)
end

function math.atan2(x, y)
	if tonumber(y) == nil or tonumber(x) == nil then
		return 0
	end

	if x == 0 then 
		if y == 0 then
			return 0
		elseif y < 0 then
			return -math.pi / 2
		else
			return math.pi / 2
		end
	end

	return math.atan(y / x) - (math.sign(x) - 1) / 2 * math.pi * math.sign(y)
end

function math.minmax(x, x_min, x_max)
	return math.min(math.max(x, x_min), x_max)
end

function math.dectobin(x, count)
	count = count or math.huge
	local r = ""
	while x > 0 and r:len() < count do
		r = (x % 2) .. r
		x = math.floor(x / 2)
	end
	if count ~= math.huge then r = string.rep("0", count - r:len()) .. r end
	return r
end

math.hexsymbols = {0,1,2,3,4,5,6,7,8,9,"a","b","c","d","e","f"}
function math.dectohex(x, count)
	count = count or math.huge
	local r = ""
	while x > 0 and r:len() < count do
		r = math.hexsymbols[x % 16 + 1] .. r
		x = math.floor(x / 16)
	end
	if count ~= math.huge then r = string.rep("0", count - r:len()) .. r end
	return r
end

--[[
	Binary to decimal
	
	Returns result of binary number string representation into decimal number.
		> math.bintodec('11011010')
		  218.0
	
	If split_count attribute is set, initial string will be splitted firstly and function will return table:
		> math.bintodec('11011010', 4)			    1101-1010
		  {13.0, 10.0}								 13   10
		> math.bintodec('11011010', 3)			    110-110-10
		  {3.0, 3.0, 2.0}							 3   3   2
]]
function math.bintodec(str, split_count)
	local str_len = str:len()
	local no_split = (split_count == nil)
	split_count = split_count or str_len
	
	if str_len % split_count > 0 then 
		for c = 1, str_len % split_count do
			str = "0"..str 
		end
		str_len = str:len()
	end
  
	local res = {}
	for c = math.floor(str_len/split_count),1,-1 do
		local string_part = str:sub(str_len-split_count*c+1,str_len-split_count*(c-1))
		local n = 0
		for pos = 1, split_count do
			n = n + tonumber(string_part:sub(split_count-pos+1,split_count-pos+1))*2^(pos-1)
		end
		res[#res+1] = n
	end  
	
	return no_split and res[1] or res
end

function math.hex_reverse(val,bytes)
	local hexString = string.format("%x", val)
	local addedHexString = string.rep("0",bytes*2-string.len(hexString)) .. hexString
	local retBytes = {}
	for i = bytes,1,-1 do
		retBytes[#retBytes+1] = tonumber(string.sub(addedHexString,i*2-1,i*2),16)
	end
	return retBytes
end

function math.mod(val1,val2)
  local t1 = val1/val2
	return val1-math.floor(math.abs(t1))*math.sign(t1)*val2
end

function table.clone(org)
	return {table.unpack(org)}
end

function table.reverse(org)
	local new_table = {}
	for i,v in pairs(org) do
		new_table[#org-i+1] = v
	end
	return new_table
end

--[[
	Checks table contents.
	
	Returns eighter value position in table or -1.
]]
function table.has_value(tbl, val)
    local val_type = type(val)
    for index, value in ipairs(tbl) do
      local val_type = type(val)
      if type(value) == val_type then
        if val_type ~= 'table' then
          if value == val then
              return index
          end
        else
          if #val == #value then
            local success = true
            for k = 1, #val do
              if val[k] == nil or value[k] == nil or val[k] ~= value[k] then
                  success = false
              end
            end
            if success then return index end
          end
        end
      end
    end
    return -1
end

function table.total(tbl) 
    local ret = 0
	for k, v in ipairs(tbl) do ret = ret + v end
	return ret
end

function table.count(tbl)
	local ret = 0
	for k, v in ipairs(tbl) do ret = ret + 1 end
	return ret
end

function table.quantile(tbl, q)
	local count = table.count(tbl)
	if count == 0 then
		return 0
	elseif count == 1 then
		return tbl[1]
	else
		local sorted = table.clone(tbl)
		table.sort(sorted)
		return sorted[math.min(math.max(math.ceil(q * count), 1), count)]
	end
end

function table.median(tbl)
	local count = table.count(tbl)
	if count == 0 then
		return 0
	elseif count == 1 then
		return tbl[1]
	else
		local sorted = table.clone(tbl)
		table.sort(sorted)
		if count % 2 == 0 then
			return (sorted[count / 2] + sorted[count / 2 + 1]) / 2
		else
			return sorted[(count + 1) / 2]
		end
	end
end

function table.mean(tbl)
	local count = table.count(tbl)
	if count == 0 then
		return 0
	else
		return table.total(tbl) / table.count(tbl)
	end
end

function table.trimmed_mean(tbl, f1, f2)
	f2 = f2 or f1 or 0.05
	f1 = f1 or 0.05
	local count = table.count(tbl)
	local c1 = math.round(count * f1)
	local c2 = math.round(count * f2)
	local sorted = table.clone(tbl)
	table.sort(sorted)
	return table.mean({table.unpack(sorted, c1 + 1, count - c2)})
end

function table.min(tbl)
	local ret = math.huge
	for k, v in ipairs(tbl) do
		ret = math.min(ret, v)
	end
	return ret
end

function table.max(tbl)
	local ret = -math.huge
	for k, v in ipairs(tbl) do
		ret = math.max(ret, v)
	end
	return ret
end

function table.interpolate(tbl, argument)
	local ret = 0	
	for i = 1, #tbl - 1 do
		if argument <= tbl[i][1] then --extrapolation by the horizontal line
			return tbl[i][2]
		end
		
		if argument >= tbl[i + 1][1] then --extrapolation by the horizontal line
			ret = tbl[i + 1][2]
		end
		
		if argument > tbl[i][1] and argument < tbl[i + 1][1] then --interpolation within the given points
			local delta_2_colum = tbl[i + 1][2] - tbl[i][2]
			local delta_1_colum = tbl[i + 1][1] - tbl[i][1]
			local argument_inc = argument - tbl[i][1]
			local ratio = argument_inc / delta_1_colum
			local ret_inc = ratio * delta_2_colum
			ret = ret_inc + tbl[i][2]
			return ret
		end
	end
	return ret
end

function table.map(tbl, f)
    local t = {}
    for k,v in pairs(tbl) do
        t[k] = f(v)
    end
    return t
end

return utilities
