utilities = utilities or {}

require "Additional.Additional"

function utilities.compress(data, dict)
  local dictionary = {}
  for i = 1, #dict do dictionary[i] = {dict[i]} end
  for i = 1,2 do dictionary[#dictionary+1] = {#dictionary+1} end
  local dict_size = #dictionary
  local val_size = 0
  
  w = {}
  result = {}
  local insert_cc = true
  for i = 1, #data do	  
    val_size = math.ceil(math.log(dict_size)/math.log(2));
    if insert_cc then
		if (i==1) then
			result[#result+1] = {#dict+1, val_size}
		else
			result[#result+1] = {#dict+1, 12}
		end
		
		insert_cc = false
	end
    
    local c = data[i]
    local wc = table.clone(w); wc[#wc+1] = c
    if table.has_value(dictionary,wc) > -1 and i < #data then
      w = table.clone(wc)
    else	  
		result[#result+1] = {table.has_value(dictionary,w), val_size}
		w = {c}
			
		if dict_size > 4095 then
			dictionary = {}
			for i = 1, #dict do dictionary[i] = {dict[i]} end
			for i = 1,2 do dictionary[#dictionary+1] = {#dictionary+1} end
			dict_size = #dictionary
			insert_cc = true
		else
			dict_size = dict_size + 1
			dictionary[dict_size] = wc
		end
		
    end
  end
  result[#result+1] = {table.has_value(dictionary,w), val_size} 
  result[#result+1] = {#dict+2, val_size} 

  for i = 1, #result do result[i][1] = result[i][1] - 1 end
  return result
end

function utilities.create_bmp(pixels, filename)
	local imageHeight = #pixels
	local imageWidth = #pixels[1]
	
	local charsArr = {66,77} 																						-- BM
	for k,v in pairs(math.hex_reverse(54 + imageWidth*imageHeight*4,4)) do charsArr[#charsArr+1] = v end 		-- File size
	for i = 1,4 do charsArr[#charsArr+1] = 0 end																	-- Unused
	for k,v in pairs(math.hex_reverse(54,4)) do charsArr[#charsArr+1] = v end									-- Headers size
	for k,v in pairs(math.hex_reverse(40,4)) do charsArr[#charsArr+1] = v end									-- DIB header size
	for k,v in pairs(math.hex_reverse(imageWidth,4)) do charsArr[#charsArr+1] = v end							-- Image width
	for k,v in pairs(math.hex_reverse(imageHeight,4)) do charsArr[#charsArr+1] = v end							-- Image height
	for k,v in pairs(math.hex_reverse(1,2)) do charsArr[#charsArr+1] = v end									-- Number of color planes
	for k,v in pairs(math.hex_reverse(32,2)) do charsArr[#charsArr+1] = v end									-- Number of bites per pixel
	for i = 1,4 do charsArr[#charsArr+1] = 0 end																	-- No pixel array compression used
	for k,v in pairs(math.hex_reverse(imageWidth*imageHeight*4,4)) do charsArr[#charsArr+1] = v end			-- Size of raw bitmap data
	for k,v in pairs(math.hex_reverse(2835,4)) do charsArr[#charsArr+1] = v end								-- Print resilution (horizontal)
	for k,v in pairs(math.hex_reverse(2835,4)) do charsArr[#charsArr+1] = v end								--                  (vertical)
	for i = 1,4 do charsArr[#charsArr+1] = 0 end																	-- Number of colors in palette
	for i = 1,4 do charsArr[#charsArr+1] = 0 end																	-- Number of important colors
	for y = imageHeight,1,-1 do
		for x = 1,imageWidth do
			charsArr[#charsArr+1] = math.floor(pixels[y][x][3]+.5)
			charsArr[#charsArr+1] = math.floor(pixels[y][x][2]+.5)
			charsArr[#charsArr+1] = math.floor(pixels[y][x][1]+.5)
			if #pixels[y][x] > 3 then charsArr[#charsArr+1] = math.floor(pixels[y][x][4]+.5) else charsArr[#charsArr+1] = 0 end
		end	
	end
	
	local str = {}
	for charI_key, charI in pairs(charsArr) do
		str[#str+1] = string.char(charI)
	end
	
	out = io.open(filename .. ".bmp", "wb")
	out:write(table.concat(str,""))
	out:close()
end

function utilities.create_animated_gif(pixels, filename, show_trace)
	if show_trace then trace("Creating ", filename) end
	
	local charsArr = {71,73,70,56,57,97} 															-- GIF89a
	for k,v in pairs(math.hex_reverse(#pixels[1][1],2)) do charsArr[#charsArr+1] = v end 		-- Image weight
	for k,v in pairs(math.hex_reverse(#pixels[1],2)) do charsArr[#charsArr+1] = v end 			-- Image height
  
	local frames = {}
	local cdictionary = {{0,0,0},{255,0,0},{0,255,0},{0,0,255},{255,255,255}}
	for f = 1, #pixels do
		local ccodes = {}
		frames[f] = {left = 0, top = 0, width = #pixels[f][1], height = #pixels[f], data = ccodes}
		for y = 1, #pixels[f] do
			ccodes[y] = {}
			for x = 1, #pixels[f][y] do 
				local color = pixels[f][y][x]
				local color_code = table.has_value(cdictionary, color)
				if color_code == -1 then
					color_code = #cdictionary+1
					cdictionary[color_code] = color
					if color_code > 255 then error("Please use less then 255 colors") end
				end
				ccodes[y][x] = color_code
			end
		end
	end
	for f = #frames, 1, -1 do
		local frame = frames[f]
		local bounds = {left = frame.left+frame.width, top = frame.top+frame.height, right = frame.left, bottom = frame.top, width = frame.width, height = frame.height}
		if f > 1 then
			for y = 1, #frame["data"] do
				for x = 1, #frame["data"][y] do
					if frame["data"][y][x] == frames[f-1]["data"][y][x] then
						frames[f]["data"][y][x] = 3
					else
						if bounds.left > x then bounds.left = x end
						if bounds.right < x then bounds.right = x end
						if bounds.top > y then bounds.top = y end
						if bounds.top < y then bounds.bottom = y end
					end
				end
			end
			bounds.left = bounds.left - 1
			bounds.top = bounds.top - 1
			bounds.width = math.max(bounds.right - bounds.left, 1)
			bounds.height = math.max(bounds.bottom - bounds.top, 1)
			bounds.right = bounds.left + bounds.width - 1
			bounds.bottom = bounds.top + bounds.height -1
			frames[f].left = bounds.left
			frames[f].top = bounds.top
			frames[f].width = bounds.width
			frames[f].height = bounds.height
		end
		local data, data_i = {}, 1
		for y = frames[f]["top"]+1, frames[f]["top"]+frames[f]["height"] do
			for x = frames[f]["left"]+1, frames[f]["left"]+frames[f]["width"] do
				data[data_i] = frames[f]["data"][y][x]
				data_i = data_i + 1
			end
		end
		frames[f]["data"] = data
	end
	local cdictionary_size = math.min(math.max(math.ceil(math.log(#cdictionary)/math.log(2))-1,2),7)           					-- Color dictionary size
	for k,v in pairs(math.bintodec("11110"..math.dectobin(cdictionary_size,3),8)) do charsArr[#charsArr+1] = v end    -- Screen logical descriptor
	for k,v in pairs({2,0}) do charsArr[#charsArr+1] = v end
	for i = 1, 2^(cdictionary_size+1) do                                                                        				-- Global palette
		if i <= #cdictionary then
			charsArr[#charsArr+1] = cdictionary[i][1]
			charsArr[#charsArr+1] = cdictionary[i][2]
			charsArr[#charsArr+1] = cdictionary[i][3]
		else
			for m = 1,3 do charsArr[#charsArr+1] = 0 end
		end
		cdictionary[i] = i
	end
	
	for k,v in pairs({33,255,11,78,69,84,83,67,65,80,69,50,46,48,3,1,0,0,0}) do charsArr[#charsArr+1] = v end

	local start_time = 0
	local end_time = os.time()
	for f = 1, #frames do
		for k,v in pairs({33,249,4,5,25,0,2,0}) do charsArr[#charsArr+1] = v end

		local compressed_data = utilities.compress(frames[f]["data"], cdictionary);
		local binary_data = ""
		local bindary_data_p = ""
		for i = 1, #compressed_data do
			binary_data = math.dectobin(compressed_data[i][1],compressed_data[i][2]) .. binary_data
		end
		local decimal_data = table.reverse(math.bintodec(binary_data,8))

		charsArr[#charsArr+1] = 44
		for k,v in pairs(math.hex_reverse(frames[f].left,2)) do charsArr[#charsArr+1] = v end 
		for k,v in pairs(math.hex_reverse(frames[f].top,2)) do charsArr[#charsArr+1] = v end
		for k,v in pairs(math.hex_reverse(frames[f].width,2)) do charsArr[#charsArr+1] = v end 
		for k,v in pairs(math.hex_reverse(frames[f].height,2)) do charsArr[#charsArr+1] = v end
		charsArr[#charsArr+1] = 0
		charsArr[#charsArr+1] = compressed_data[1][2]-1

		local written_data = 0
		while #decimal_data - written_data > 0 do
			charsArr[#charsArr+1] = math.min(#decimal_data - written_data, 254)
			for k = written_data+1, math.min(written_data + 254, #decimal_data) do
				charsArr[#charsArr+1] = decimal_data[k]
			end
			written_data = math.min(written_data + 254, #decimal_data)
		end

		charsArr[#charsArr+1] = 0

		start_time = end_time
		end_time = os.time()
		elapsed_time = os.difftime(end_time,start_time)
		if show_trace then trace("Frame: ", f, " of ", #frames, " (", elapsed_time, "s)") end
	end

	charsArr[#charsArr+1] = 59

	local str = { }	
	for charI_key, charI in pairs(charsArr) do
		str[#str+1] = string.char(charI)
	end
	local field = ""
	if plot_type == 0 then
		field = "color_"
	elseif plot_type == 1 then
		field = "distance_"
	end
	
	out = io.open(filename .. ".gif", "wb")
	out:write(table.concat(str,""))
	out:close()

	if show_trace then trace(filename, " created") end
end

function utilities.create_gif(pixels, filename, show_trace)
	utilities.create_animated_gif({pixels}, filename, show_trace)
end

return utilities