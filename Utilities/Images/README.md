# Image generation

Utilities module contains special functions for image files generation.

```lua
utilities.create_bmp(pixels, filename)
utilities.create_gif(pixels, filename)
utilities.create_animated_gif(frames, filename)
```

`pixels` is a two-dimensional array of pixels.  
Each pixel is a list that contains 3 or 4 numbers in 0 – 255 range:
```lua
{R, G, B}
{R, G, B, A}
```


Simple pixel array generation

```lua
require "Utilities"

local red = {255, 0, 0}
local white = {255, 255, 255}
local pixels = {
	{white, white, white,   red, red},
	{white, white,   red, white, red},
	{white,   red, white, white, red},
	{  red, white, white, white, red},
	{  red,   red,   red,   red, red}
}

utilities.create_bmp(pixels, "test_1")
utilities.create_gif(pixels, "test_1")
```

![test_1](example_1.png)


Consider that GIF-image palette is limited by colors count (< 255).  
Following example will throw exception on GIF generation.

```lua
require "Utilities"

pixels = {}
for i = 1, 255 do
	pixels[i] = {}
	for j = 1, 255 do
		pixels[i][j] = {i, j, 255}
	end
end

utilities.create_bmp(pixels, "test_2")
utilities.create_gif(pixels, "test_2")
```

![test_2](example_2.png)

Simple color quantization can solve this problem:

```lua
require "Utilities"

pixels = {}
for i = 1, 255 do
	pixels[i] = {}
	for j = 1, 255 do
		pixels[i][j] = {math.floor(i / 20) * 20 + 1, math.floor(j / 20) * 20 + 1, 255}
	end
end

utilities.create_gif(pixels, "test_3")
```

![test_3](example_3.png)

It is possible to generate animated GIF-images. For this purpose consider `frames` as array of `pixels`:

```lua
require "Utilities"

local white = {255, 255, 255}
local black = {0, 0, 0}

frames = {}
for f = 1, 10 do
	frames[f] = {}
	for i = 1, 251 do
		frames[f][i] = {}
		for j = 1, 251 do
			frames[f][i][j] = white
		end
	end
	local r = 50 + 50 * math.cos((f - 5) / 5 * math.pi / 2)
	for a = 0, 2 * math.pi, .001 * math.pi do
		frames[f][math.round(r * math.cos(a) + 125)][math.round(r * math.sin(a) + 125)] = black
	end
end

utilities.create_animated_gif(frames, "test_4")
```

![test_4](example_4.gif)
